var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var User = new Schema({
    status: {
        type: Number,
        default: 0
    },
    fbId: {
        type: String,
        default: ''
    },
    name: {
        type: String,
        default: ''
    },
    brokerage: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    phoneNumber: {
        type: String,
        default: ''
    },
    allowToCall: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("User", User);