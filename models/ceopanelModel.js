var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Author = new Schema({
    img: String,
    author: String
});

var Ceopanel = new Schema({
    date:{
        dayOfWeek: String,
        month: String,
        number: String,
    },
    time:{
        start: String,
        end: String
    },
    title: String,
    description: String,
    _authorId: [Schema.Types.ObjectId],
});

module.exports = mongoose.model("Ceopanel", Ceopanel);