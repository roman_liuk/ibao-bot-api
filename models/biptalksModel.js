var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var BipTalk = new Schema({
    date: {
        dayOfWeek: String,
        month: String,
        number: String
    },
    time: String,
    title: String,
    _authorId: Schema.Types.ObjectId,
});

module.exports = mongoose.model("BipTalk", BipTalk);