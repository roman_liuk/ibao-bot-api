var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Author = new Schema({
    img: String,
    author: String,
    info: String,
    blog: String
});

module.exports = mongoose.model("Author", Author);