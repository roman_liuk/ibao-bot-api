var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Agenda = new Schema({
    date: {
        dayOfWeek: String,
        month: String,
        number: String
    },
    time:{
        start: String,
        end: String
    },
    description: String
});

module.exports = mongoose.model("Agenda", Agenda);