var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Pricing = new Schema({
    full: Array,
    laCarte: Array
});

module.exports = mongoose.model("Pricing", Pricing);