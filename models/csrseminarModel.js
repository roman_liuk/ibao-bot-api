var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Csrseminar = new Schema({
    title: String,
    _authorId: Schema.Types.ObjectId,
    date:{
        dayOfWeek: String,
        month: String,
        number: String,
    },
    time:{
        start: String,
        end: String
    },
});

module.exports = mongoose.model("Csrseminar", Csrseminar);