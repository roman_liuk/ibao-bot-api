var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Excellence = new Schema({
    title: String,
    _authorId: Schema.Types.ObjectId,
    submitNomination: String,
    date: {
        dayOfWeek: String,
        month: String,
        number: String
    },
    time:{
        start: String,
        end: String
    },
});

module.exports = mongoose.model("Excellence", Excellence);