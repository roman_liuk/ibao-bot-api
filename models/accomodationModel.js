var mongoose = require('../lib/dbConnectorLib');
var Schema = mongoose.Schema;

var Accomodation = new Schema({
    roomTypes:[String],
    hotel: String,
    hotelLink: String,
    onlineReservation: String,
    phone: String,
    groupCode: String
});

module.exports = mongoose.model("Accomodation", Accomodation);