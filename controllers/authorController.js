var express = require('express');
var Author = require('../models/authorModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET authors listing. */
function getAuthor(res, name) {
    if (name) {
        Author
            .findOne({
                'author': {
                    '$regex': name,
                    '$options': 'i'
                }
            })
            .select('-_id -__v')
            .exec(function (err, author) {
                log.info(JSON.stringify(author, 8, " "));

                if (err || !author) {
                    res.statusCode = 500;
                    return res.end(err);
                } else {
                    res.json(author);
                    return res.end();
                }
            });
    } else {
        Author
            .find({})
            .select('-_id -__v')
            .exec(function (err, authors) {
                if (err) {
                    res.statusCode = 500;
                    return res.end(err);
                } else {
                    res.json(authors);
                    return res.end();
                }
            });
    }
}

router.get('/', function(req, res, next) {
    if(Object.keys(req.query).length){
        getAuthor(res, req.query.name);
    } else {
        getAuthor(res);
    }
});

/* DELETE authors*/
router.delete('/', function (req, res) {
    if(req.body.secret == "gns" ){
        Author
            .remove({}, function(err){
                if(err) {
                    res.statusCode = 500;
                    res.end('Server error');
                    return log.warn(err);
                }

                log.info("Authors collection cleared");

                res.statusCode = 200;
                res.end('OK');
            });
    }
});

module.exports = router;