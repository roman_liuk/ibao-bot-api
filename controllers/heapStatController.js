var express = require('express');
var fs = require('fs');

var router = express.Router();

router.get('/', function(req, res, next) {
    fs.readFile('./stats.txt', function (err, data) {
        if(err){
            res.end('Can not to read heap stat');
        } else {
            res.end(data);
        }
    });
});

module.exports = router;
