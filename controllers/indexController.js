var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.end("IBAO Facebook bot API");
});

module.exports = router;
