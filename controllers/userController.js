var express = require('express');
var User = require('../models/userModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

function getUsers(fbId, cb){
    if(typeof fbId == 'string' && fbId.length){
        User
            .findOne({
                'fbId': fbId
            })
            .select('-__v')
            .exec(function (err, user) {
                if (err || !user) {
                    cb(new Error('No such user'));
                } else {
                    cb(null, user);
                }
            });
    } else {
        User
            .find({})
            .select('-__v')
            .exec(function (err, users) {
                log.info(JSON.stringify(users, 8, " "));

                if (err || !users.length) {
                    cb(new Error('No users'));
                } else {
                    cb(null, users);
                }
            });
    }
}

function joinObjects(one, two){
    if(Object.keys(one).length >= Object.keys(two).length){
        for(var key in one){
            if(two[key] && (two[key].length > one[key].length)){
                one[key] = two[key];
            }
        }
    } else {
        for(var key in two){
            if(one[key]) {
                if(two[key].length > one[key].length)
                    one[key] = two[key];
            } else {
                one[key] = two[key];
            }
        }
    }

    return one;
}

router.get('/', function(req, res, next) {
    if(Object.keys(req.body).length && req.body.fbId){
        getUsers(req.body.fbId, function(err, user){
            if(err){
                res.statusCode = 500;
                res.end(err.toString());
            }

            res.json(user);
        });
    } else {
        getUsers(null, function(err, users){
            if(err){
                res.statusCode = 500;
                res.end(err.toString());
            } else {
                res.json(users);
            }
        });
    }

});

router.post('/', function(req, res, next) {
    if(req.body && req.body.fbId){
        getUsers(req.body.fbId, function(err, user){
            if(err){
                user = new User({
                    fbId: req.body.fbId
                });
            } else {
                switch(user.status){
                    case 0: {
                        user.name = req.body.text;
                    } break;
                    case 1: {
                        user.brokerage = req.body.text;
                    } break;
                    case 2: {
                        user.email = req.body.text;
                    } break;
                    case 3: {
                        user.phoneNumber = req.body.text;
                    } break;
                    case 4: {
                        user.allowToCall = req.body.text;
                    } break;
                }
                user.status = user.status + 1;
            }

            log.info(JSON.stringify(user._doc,8));

            user.save(function(err, savedUser){
                if(err){
                    log.warn(err);
                    res.statusCode = 500;
                    res.end('Server error');

                } else {
                    res.statusCode = 200;
                    res.end(savedUser.status+'');
                }
            });
        });
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

router.delete('/', function (req, res) {
    if(req.body.secret == "gns" && req.body && req.body.fbId){
        User
            .remove({fbId: req.body.data.fbId + ''}, function(err){
                if(err) {
                    res.statusCode = 500;
                    res.end('Server error');
                    return log.warn(err);
                }

                res.statusCode = 200;
                res.end('OK');
            });
    }
});

module.exports = router;