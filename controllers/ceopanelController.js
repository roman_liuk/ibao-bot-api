var express = require('express');
var Ceopanel = require('../models/ceopanelModel');
var Author = require('../models/authorModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET ceopanel listing. */
router.get('/', function(req, res, next) {
    var result;

    Ceopanel
        .find({})
        .select('-_id -__v')
        .exec(function(err, data){
            if(err) {
                res.statusCode = 500;
                return res.end(err);
            }

            data[0]._authorId.forEach(function(authorId, i, arr){
                Author
                    .find({_id: authorId})
                    .select('-_id -__v')
                    .exec(function(err, author){
                        if(err) {
                            res.statusCode = 500;
                            return res.end(err);
                        }

                        if(!data[0]._doc.authors)
                            data[0]._doc.authors = [];

                        data[0]._doc.authors.push(author[0]);

                        if(i == arr.length-1){
                            delete data[0]._doc._authorId;

                            res.json(data);
                            return res.end();
                        }
                    });
            });

        });
});

/* POST ceopanel listing. */
function joinObjects(one, two){
    if(Object.keys(one).length >= Object.keys(two).length){
        for(var key in one){
            if(two[key] && (two[key].length > one[key].length)){
                one[key] = two[key];
            }
        }
    } else {
        for(var key in two){
            if(one[key]) {
                if(two[key].length > one[key].length)
                    one[key] = two[key];
            } else {
                one[key] = two[key];
            }
        }
    }

    return one;
}

function passCeopanel(req, res){
    Ceopanel.remove({}, function(err){
        if(err) {
            res.statusCode = 500;
            res.end("Trouble on server side");
            return log.warn("Can't remove ceopanel collection");
        }

        log.info("Ceopanel collection removed");

        var authorsId = [];
        req.body.data.authors.forEach(function(item, i, arr){
            Author
                .findOne({'author':{
                    '$regex': item.author,
                    '$options': 'i'
                }})
                .select('-__v')
                .exec(function(err, author){
                    if(err) {
                        res.statusCode = 500;
                        return res.end(err);
                    }

                    if(!author){
                        author = new Author(item);
                    } else {
                        author = joinObjects(author, item);
                    }

                    author.save(function(err, saved){
                        if(err){
                            log.warn(err);
                            res.statusCode = 500;
                            res.end('Server error');
                        } else {
                            authorsId.push(saved._id);

                            if (authorsId.length == arr.length) {
                                delete req.body.data.authors;
                                req.body.data._authorId = authorsId;

                                var ceopanel = new Ceopanel(req.body.data);
                                ceopanel.save(function(err, saved){
                                    if(err){
                                        log.warn(err);

                                        res.statusCode = 500;
                                        res.end('Server error');
                                    } else {
                                        log.info("Ceopanel saved to db");

                                        res.statusCode = 200;
                                        res.end('OK');
                                    }
                                });
                            }
                        }
                    });
                });
        });
    });
}

router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data){
        passCeopanel(req, res);
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;