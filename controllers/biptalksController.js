var express = require('express');
var BipTalk = require('../models/biptalksModel');
var Author = require('../models/authorModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET biplalk listing. */

function getBipTalksFromDB(res, options){
    BipTalk
        .find({})
        .select('-_id -__v')
        .exec(function(err, data){
            if(err) {
                res.statusCode = 500;
                return res.end(err);
            }

            data.forEach(function(record, i, arr){
                Author
                    .find({_id: record._authorId})
                    .select('-_id -__v')
                    .exec(function(err, author){
                        if(err) {
                            res.statusCode = 500;
                            return res.end(err);
                        }

                        delete record._doc._authorId;
                        record._doc.author = author[0];

                        if(i == arr.length-1){
                            res.json(data);
                            res.end();
                        }
                    });
            });
        });
}

router.get('/', function(req, res, next) {
    if(Object.keys(req.body).length){
        //TODO: add some options to db request
        getBipTalksFromDB(res);
    } else {
        getBipTalksFromDB(res);
    }
});


/* POST biplalk listing. */
function joinObjects(one, two){
    if(Object.keys(one).length >= Object.keys(two).length){
        for(var key in one){
            if(two[key] && (two[key].length > one[key].length)){
                one[key] = two[key];
            }
        }
    } else {
        for(var key in two){
            if(one[key]) {
                if(two[key].length > one[key].length)
                    one[key] = two[key];
            } else {
                one[key] = two[key];
            }
        }
    }

    return one;
}

function passBipTalksToDB(req, res){
    BipTalk.remove({}, function(err){
        if(err) {
            res.statusCode = 500;
            res.end("Trouble on server side");
            return log.warn("Can't remove biptalk collection");
        }

        log.info("Biptalk collection removed");

        req.body.data.forEach(function(item, i){
            Author
                .findOne({'author':{
                    '$regex': item.authors[0].author,
                    '$options': 'i'
                }})
                .select('-__v')
                .exec(function(err, author){
                    if(err) {
                        res.statusCode = 500;
                        return res.end(err);
                    }

                    if(!author){
                        author = new Author(item.authors[0]);
                    } else {
                        author = joinObjects(author, item.authors[0]);
                    }

                    author.save(function(err, saved){
                        if(err){
                            log.warn(err);
                            res.statusCode = 500;
                            res.end('Server error');
                        } else {
                            delete item.authors;
                            item._authorId = saved._id;

                            var biptalk = new BipTalk(item);
                            biptalk.save(function(err, saved){
                                if(err){
                                    log.warn(err);

                                    res.statusCode = 500;
                                    res.end('Server error');
                                } else {
                                    log.info("Save new biptalk item to db");

                                    res.statusCode = 200;
                                    res.end('OK');
                                }
                            });
                        }
                    });
                });
        });
    });
}

router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data.length){
        passBipTalksToDB(req, res);
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;