var express = require('express');
var Agenda = require('../models/agendaModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET agenda listing. */
router.get('/', function(req, res, next) {
    if(Object.keys(req.body).length){
        //TODO: add some options to db request
        Agenda
            .find({})
            .select('-_id -__v')
            .exec(function(err, data){
                if(err) {
                    res.statusCode = 500;
                    return res.end(err);
                }

                res.json(data);
                return res.end();
            });
    } else {
        Agenda
            .find({})
            .select('-_id -__v')
            .exec(function(err, data){
                if(err) {
                    res.statusCode = 500;
                    return res.end(err);
                }

                res.json(data);
                return res.end();
            });
    }
});

/* POST agenda listing. */
router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data.length){
        Agenda.remove({}, function(err){
            if(err) {
                res.statusCode = 500;
                res.end("Trouble on server side");
                return log.warn("Can't remove agenda collection");
            }

            log.info("Agendas collection removed");

            req.body.data.forEach(function(item, i){
                var agenda = new Agenda(item);
                agenda.save(function(err, saved){
                    if(err)
                        log.warn(err);
                    else
                        log.info("Save new agenda item to db");
                });
            });

            res.statusCode = 200;
            res.end('OK');
        });
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;
