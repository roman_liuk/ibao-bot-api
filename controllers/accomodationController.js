var express = require('express');
var Accomodation = require('../models/accomodationModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET accomodation listing. */
router.get('/', function(req, res, next) {
    Accomodation
        .find({})
        .select('-_id -__v')
        .exec(function(err, data){
            if(err) {
                res.statusCode = 500;
                return res.end(err);
            }

            res.json(data);
            return res.end();
        });
});

/* POST accomodation listing. */
router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data){
        Accomodation.remove({}, function(err){
            if(err) {
                res.statusCode = 500;
                res.end("Trouble on server side");
                return log.warn("Can't remove accomodation collection");
            }

            log.info("Accomodation collection removed");

            var accomodation = new Accomodation(req.body.data);

            accomodation.save(function(err, saved){
                if(err) return log.warn("Can'tsave accomodation to db\n", err);

                return log.info("Accomodation saved to db");
            });

            res.statusCode = 200;
            res.end('OK');
        });
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;