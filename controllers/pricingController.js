var express = require('express');
var Pricing = require('../models/pricingModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET pricing listing. */
router.get('/', function(req, res, next) {
    Pricing
        .find({})
        .select('-_id -__v')
        .exec(function(err, data){
            if(err) {
                res.statusCode = 500;
                return res.end(err);
            }

            res.json(data);
            return res.end();
        });
});

/* POST pricing listing. */
router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data){
        Pricing.remove({}, function(err){
            if(err) {
                res.statusCode = 500;
                res.end("Trouble on server side");
                return log.warn("Can't remove pricing collection");
            }

            log.info("Pricing collection removed");

            var pricing = new Pricing(req.body.data);
            pricing.save(function(err, saved){
                if(err) return log.warn("Can't save pricing to db");

                log.info("Pricing saved to db");

                res.statusCode = 200;
                res.end('OK');
            });
        });
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;