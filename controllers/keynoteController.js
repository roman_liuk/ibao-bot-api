var express = require('express');
var Keynote = require('../models/keynoteModel');
var Author = require('../models/authorModel');
var router = express.Router();
var log = require('../lib/logLib')(module);

/* GET keynote listing. */
function getKeynote(res) {
    Keynote
        .find({})
        .select('-_id -__v')
        .exec(function(err, data){
            if(err) {
                res.statusCode = 500;
                return res.end(err);
            }

            Author
                .find({_id: data[0]._authorId})
                .select('-_id -__v')
                .exec(function(err, author){
                    if(err) {
                        res.statusCode = 500;
                        return res.end(err);
                    }

                    delete data[0]._doc._authorId;
                    data[0]._doc.author = author[0];

                    res.json(data);
                    return res.end();
                });
        });
}

router.get('/', function(req, res, next) {
    if(Object.keys(req.body).length){
        //TODO: add some options to db request
        getKeynote(res);
    } else {
        getKeynote(res);
    }
});

/* POST keynote listing. */
function joinObjects(one, two){
    if(Object.keys(one).length >= Object.keys(two).length){
        for(var key in one){
            if(two[key] && (two[key].length > one[key].length)){
                one[key] = two[key];
            }
        }
    } else {
        for(var key in two){
            if(one[key]) {
                if(two[key].length > one[key].length)
                    one[key] = two[key];
            } else {
                one[key] = two[key];
            }
        }
    }

    return one;
}

function passKeynote(req, res){
    Keynote.remove({}, function(err){
        if(err) {
            res.statusCode = 500;
            res.end("Trouble on server side");
            return log.warn("Can't remove keynote collection");
        }

        log.info("Keynote collection removed");

        Author
            .findOne({'author':{
                '$regex': req.body.data.authors[0].author,
                '$options': 'i'
            }})
            .select('-_id -__v')
            .exec(function(err, author){
                if(err) {
                    res.statusCode = 500;
                    return res.end(err);
                }

                if(!author){
                    author = new Author(req.body.data.authors[0]);
                } else {
                    author = joinObjects(author, req.body.data.authors[0]);
                }

                author.save(function(err, saved){
                    if(err){
                        log.warn(err);
                        res.statusCode = 500;
                        res.end('Server error');
                    } else {
                        delete req.body.data.authors;
                        req.body.data._authorId = saved._id;

                        var keynote = new Keynote(req.body.data);
                        keynote.save(function(err, saved){
                            if(err){
                                log.warn(err);

                                res.statusCode = 500;
                                res.end('Server error');
                            } else {
                                log.info("Save new keynote item to db");

                                res.statusCode = 200;
                                res.end('OK');
                            }
                        });
                    }
                });
            });
    });
}

router.post('/', function(req, res, next) {
    if(req.body.secret == "gns" && req.body.data){
        passKeynote(req, res);
    } else {
        res.statusCode = 503;
        res.end('Bad request');
    }
});

module.exports = router;