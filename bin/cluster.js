var cluster = require('cluster');
var fs = require('fs');
var log = require('./../lib/logLib')(module);

process.title = "ibao-bot-api";


var config = {
    numWorkers: require('os').cpus().length
};

cluster.setupMaster({
    exec: "./bin/worker.js",
    //execArgv: ["--expose-gc"]
});

// Fork workers as needed.
for (var i = 0; i < config.numWorkers; i++)
    cluster.fork()

function generateHeapDumpAndStats(){
    try {
        global.gc();
    } catch (e) {
        log.info("You must run program with 'node --expose-gc index.js' or 'npm start'");
        process.exit();
    }

    var heapUsed = process.memoryUsage().heapUsed;

    fs.appendFile("stats.txt", heapUsed+"\n", function(err) {
        if(err) {
            log.warn(err);
        }
    });
}

setInterval(generateHeapDumpAndStats, 2000);