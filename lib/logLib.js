var winston = require('winston');
var config = require('../config');

for (key in winston.loggers.loggers) {
    winston.loggers.loggers[key].remove(winston.transports.Console);
}

function getLogger(module) {
    var path = module.filename.split('/').slice(-2).join('/');

    if(config.get('env') == 'development'){
        return new winston.Logger({
            transports : [
                new winston.transports.Console({
                    colorize:   true,
                    level:      config.get('env') == 'development' ? 'debug' : 'error',
                    label:      path
                })
            ]
        });
    } else if(config.get('env') == 'production'){
        return new winston.Logger({
            transports : [
                new (winston.transports.File)({
                    name: 'info-file',
                    filename: 'filelog-info.log',
                    level: 'info',
                    label: path
                }),
                new (winston.transports.File)({
                    name: 'error-file',
                    filename: 'filelog-error.log',
                    level: 'warn',
                    label: path
                })
            ]
        });
    }
}
module.exports = getLogger;