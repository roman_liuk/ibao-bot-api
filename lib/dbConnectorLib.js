var path = require('path');
var mongoose = require('mongoose');
var config = require('../config');
var log = require('./logLib')(module);

mongoose.connect(config.get('db:url'));

var db = mongoose.connection;

db.on('error', function(){
    log.error("Can't connect to database.");
});

db.once('open', function callback () {
    log.info("Connected to database.");
});


module.exports = mongoose;