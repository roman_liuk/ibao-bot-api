var config = require("../config");

module.exports = function(app){
    app.use(config.get('urls:authors'), require('../controllers/authorController'));
    app.use(config.get('urls:agenda'), require('../controllers/agendaController'));
    app.use(config.get('urls:biptalks'), require('../controllers/biptalksController'));
    app.use(config.get('urls:accommodation'), require('../controllers/accomodationController'));
    app.use(config.get('urls:ceopanel'), require('../controllers/ceopanelController'));
    app.use(config.get('urls:csrseminar'), require('../controllers/csrseminarController'));
    app.use(config.get('urls:keynote'), require('../controllers/keynoteController'));
    app.use(config.get('urls:excellence'), require('../controllers/excellenceController'));
    app.use(config.get('urls:pricing'), require('../controllers/pricingController'));
    app.use(config.get('urls:users'), require('../controllers/userController'));
    app.use('/heap', require('../controllers/heapStatController'));
    app.use('/', require('../controllers/indexController'));
};
